<?php
class WebUser extends CWebUser {
	private $_user;

	private function loadUser($user_id) {
		if(!Yii::app()->user->isGuest) {
			$this->_user = User::model()->findByPk($user_id);
		} else {
			$this->_user = false;
		}

		return $this->_user;
	}
}